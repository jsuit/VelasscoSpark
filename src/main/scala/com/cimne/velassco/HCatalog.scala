package com.cimne.velassco

import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{ConnectionFactory, Get, Scan}
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapreduce.TableInputFormat
import org.apache.hadoop.hbase.util.{Base64, Bytes, Hash}
import org.apache.hadoop.hbase.filter.{ColumnPrefixFilter, MultipleColumnPrefixFilter}

import scala.collection.JavaConversions._
import org.apache.hadoop.hbase.protobuf.generated._
import org.apache.hadoop.hbase.protobuf._
import org.apache.spark.SparkContext

import scala.reflect.ClassTag
import scala.collection.immutable.HashMap
import scala.collection.mutable.ArrayBuffer

object HCatalog {
  type RowKey = Array[Byte]
  type CellValue = Array[Byte]
  type ModelRecord = (RowKey, scala.collection.mutable.Map[String,CellValue])
  type HKeyRaw = org.apache.hadoop.hbase.io.ImmutableBytesWritable
  type HResultRaw = org.apache.hadoop.hbase.client.Result
  type HBaseRDD = org.apache.spark.rdd.RDD[(HKeyRaw, HResultRaw)]

  def bytesToHex(bytes : Array[Byte]) = Bytes.toBytes(bytes.map{ b => String.format("%02x", new java.lang.Integer(b & 0xff)) }.mkString)

  def hexToByte(b: Byte): Byte = if(b>=97) (b-97+10).toByte else if (b>=65) (b-65+10).toByte else (b-48).toByte
  def hexToBytes(bytes: Array[Byte]): Array[Byte] = (for (i <- 0 until bytes.length / 2) yield ((hexToByte(bytes(2 * i)) << 4) | hexToByte(bytes(2 * i + 1))).toByte).toArray
  /*
  {
    var result = new ArrayBuffer[Byte]
    for (i <- 0 until bytes.length / 2) {
      result += (((bytes(2 * i) - 48) << 4) | (bytes(2 * i + 1) - 48)).toByte
    }
    result
  }*/

  def getHKeyMeshFromHKeyResult(hkey: ImmutableBytesWritable): ImmutableBytesWritable = {
    val rk0 = hkey.get()
    val len = rk0.length
    //println( "rk0 = " + Bytes.toString(rk0))
    val partition:Int = Bytes.toInt(hexToBytes(rk0.slice(len-8,len)))
    val modelKey = rk0.slice(0,32)
    val newKey = getPartitionRowKey_Data(modelKey,"",0,partition)
    //println( "partition = " + partition + " => " + newKey)
    new ImmutableBytesWritable(newKey)
  }

  def getModelIDAsString( r: ModelRecord ): String = {
    Bytes.toString( r._1 )
  }

  def getModelName(r: ModelRecord): String = {
    Bytes.toString(r._2("nm"))
  }

  def getNumberOfPartitions( r: ModelRecord ): Integer = {
    Bytes.toInt( r._2("np") )
  }

  // Result "PARTITION INDEX" "Kratos" 21 Scalar OnNodes
  def getPrefixRowKey_Data(modelId: RowKey, nameAnalysis: String, step: Double ) : RowKey = {
    val lengthName = nameAnalysis.length
    val bytesLengthName = bytesToHex(Bytes.toBytes(lengthName))
    val bytesStep = bytesToHex(Bytes.toBytes(step))

    Array.concat( modelId, bytesLengthName, Bytes.toBytes(nameAnalysis), bytesStep )
  }

  def getPartitionRowKey_Data(modelId: RowKey, nameAnalysis: String, step: Double, partition: Integer ) : RowKey = {
    val lengthName = nameAnalysis.length
    val bytesLengthName = bytesToHex(Bytes.toBytes(lengthName))

    val bytesStep = bytesToHex(Bytes.toBytes(step))
    val bytesPartition = bytesToHex(Bytes.toBytes(partition))

    Array.concat( modelId, bytesLengthName, Bytes.toBytes(nameAnalysis), bytesStep, bytesPartition )
  }

  def buildGetResult_Metadata(modelId: RowKey, analysis: String, step: Double ) : Get = {
    val rowKey = getPrefixRowKey_Data(modelId, analysis, step)
    val get = new Get(rowKey)
    get.addFamily(Bytes.toBytes("R"))
  }

  def buildScanMesh(modelId: RowKey, meshId: String): Scan = {
    val scanAll = new Scan()
    scanAll.setRowPrefixFilter(modelId)
    scanAll.addFamily(Bytes.toBytes("M"))
    scanAll.setFilter(new MultipleColumnPrefixFilter(Array("c".getBytes, Bytes.toBytes(meshId+"cn"))))
    scanAll.setMaxVersions(1)
  }

  def buildScanElements_Data(modelId: RowKey, meshId: String) : Scan = {
    val scan = new Scan()
    scan.setRowPrefixFilter(modelId)
    scan.addFamily(Bytes.toBytes("M"));
    scan.setFilter(new ColumnPrefixFilter(Bytes.toBytes(meshId+"cn"))).setMaxVersions(1)
    scan
  }

  def buildScanNodes_Data(modelId: RowKey, partition: Integer ) : Scan = {
    val scan = if( partition < 0 ) {
      val scanAll = new Scan()
      scanAll.setRowPrefixFilter( modelId )
      scanAll.addFamily( Bytes.toBytes("M" ) )
      scanAll
    } else {
      val partRowKey = getPartitionRowKey_Data(modelId, "", 0, partition)
      val getOne = new Get(partRowKey)
      getOne.addFamily(Bytes.toBytes("M"))
      val scanOne = new Scan(getOne)
      scanOne
    }
    scan.setFilter(new ColumnPrefixFilter( Bytes.toBytes("c"))).setMaxVersions(1)
    scan
  }
}

import HCatalog._

/**
  * Created by jorge on 18/07/16.
  */
class HCatalog(val sc: SparkContext, val suffix : String = "_V4CIMNE") {

  @transient val hconf = HBaseConfiguration.create()
  //hconf.set("hbase.client.scanner.timeout.period","300000");

  val connection = ConnectionFactory.createConnection(hconf)
  val admin = connection.getAdmin()
  var listOfModels : Array[ModelRecord] = Array()

  def getTableNames = admin.listTableNames.map( x => Bytes.toString(x.getName) )
  def getTableModelsName = "VELaSSCo_Models" + suffix
  def getTableDataName = "Simulations_Data" + suffix
  def getTableMetaDataName = "Simulations_Metadata" + suffix

  def getNumberOfModels = listOfModels.length

  def printMeshes(keyModel: Array[Byte]): Unit = {
    val rddMeshes = getRddMesh_MetaData(keyModel).flatMap(_._2.getFamilyMap(Bytes.toBytes("M")))

    val rddMeshes1 = rddMeshes.map( col => {
      val cprefix = Bytes.toString(col._1.slice(0,7))
      val field = Bytes.toString(col._1.slice(7,9))
      val value = if(field == "nc" || field=="ne") Bytes.toLong(col._2)
      else if(field == "nn") Bytes.toInt(col._2)
      else if(field == "bb") "BBox"
      else Bytes.toString(col._2)
      (cprefix,(field, value))
    })
    rddMeshes1.collect.foreach( it => {
      println(it._1 + " = " + it._2._1 + ", " + it._2._2)
    })
  }

  def setHBaseClientScannerTimeout( tm: Int ) = hconf.set("hbase.client.scanner.timeout.period", tm.toString)

  def printModelIDs: Unit = {
    if( listOfModels.size == 0 ) refreshListOfModels
    val leftAlignFormat0 = "| %5s | %-40s | %-32s |%n"
    val leftAlignFormat1 = "| %5d | %-40s | %-32s |%n"
    val dash5 = "-"*(5+2)
    val dash40 = "-"*(40+2)
    val dash32 = "-"*(32+2)
    println("+" + dash5 + "+"+ dash40 + "+" + dash32 + "+")
    printf(leftAlignFormat0, "Index", "Model Name", "Row Key")
    println("+" + dash5 + "+"+ dash40 + "+" + dash32 + "+")
    var idx = 0
    listOfModels.foreach( m => {
      // println("name = '" + HCatalog.getModelName(m) + "', rowkey = '" + HCatalog.getModelIDAsString(m) + "'"))
      printf(leftAlignFormat1, idx, HCatalog.getModelName(m), HCatalog.getModelIDAsString(m))
      idx += 1
    })
    println("+" + dash5 + "+"+ dash40 + "+" + dash32 + "+")
  }

  def getListOfModelNames = {
    if( listOfModels.size == 0 ) refreshListOfModels
    listOfModels.map( HCatalog.getModelName(_) )
  }

  def getModelKey(index: Int ): RowKey = {
    if( listOfModels.size == 0 ) refreshListOfModels
    listOfModels(index)._1
  }

  def getNumberOfPartitions( index: Int ): Int = {
    if( listOfModels.size == 0 ) refreshListOfModels
    HCatalog.getNumberOfPartitions( listOfModels(index) )
  }

  def getNumberOfPartitions(key: RowKey): Int = {
    listOfModels.foreach(rec => {
      if(java.util.Arrays.equals(rec._1,key)) {
        return HCatalog.getNumberOfPartitions(rec)
      }
    })
    return 0
  }

  def getModelName( index: Int): String = {
    if( listOfModels.size == 0 ) refreshListOfModels
    HCatalog.getModelName( listOfModels(index) )
  }

  def refreshListOfModels: Unit = {
    println( "[refreshListOfModels] -- querying list of models")
    hconf.set(TableInputFormat.INPUT_TABLE, getTableModelsName);
    val rddModels = sc.newAPIHadoopRDD(hconf, classOf[TableInputFormat], classOf[HKeyRaw], classOf[HResultRaw])
    val rdd1 = rddModels.map( row => (row._1.get(),
                                      row._2.getFamilyMap( Bytes.toBytes("Properties") ).map( col => Bytes.toString(col._1)->col._2)))
    println( "[refreshListOfModels] -- collecting list of models" )
    listOfModels = rdd1.collect
  }

  /*
  import org.apache.hadoop.hbase.spark.HBaseContext
  @transient val hconf = HBaseConfiguration.create()
  val hbaseContext = new HBaseContext(sc, hconf)
  hbaseContext.hbaseRDD(TableName.valueOf(tableModels), scan);
   */
  def getRdd( scan: Scan, table: String ) : HBaseRDD = {
    val proto = ProtobufUtil.toScan(scan);
    val strScan = Base64.encodeBytes(proto.toByteArray());
    hconf.set(TableInputFormat.INPUT_TABLE, table);
    hconf.set(TableInputFormat.SCAN, strScan);

    sc.newAPIHadoopRDD(hconf, classOf[TableInputFormat], classOf[HKeyRaw], classOf[HResultRaw])
  }

  def getRdd_Data(scan: Scan): HBaseRDD = getRdd(scan, getTableDataName)

  def getRdd_MetaData(scan: Scan): HBaseRDD = getRdd(scan, getTableMetaDataName)

  /*
  def getRddElements_Data(modelId: RowKey, partition: Integer = -1) : HBaseRDD = {
    @transient val scan = buildScanElements_Data( modelId, partition )
    val proto = ProtobufUtil.toScan(scan);
    val strScan = Base64.encodeBytes(proto.toByteArray());
    hconf.set(TableInputFormat.INPUT_TABLE, getTableDataName);
    hconf.set(TableInputFormat.SCAN, strScan);
    sc.newAPIHadoopRDD(hconf, classOf[TableInputFormat], classOf[HKeyRaw], classOf[HResultRaw])
  }
  */

  def getRddMesh_Data(modelId: RowKey, meshId: String): HBaseRDD = {
    @transient val scan = buildScanMesh(modelId,meshId)
    getRdd_Data( scan )
  }

  def getRddMesh_MetaData(modelId: RowKey): HBaseRDD = {
    @transient val scan = buildScanMesh(modelId, "m")
    getRdd_MetaData( scan )
  }

  def getRddResult_Data_ByName(modelId: RowKey, analysis: String, step: Double, nameResult: String): HBaseRDD = {
    @transient val scan = new Scan()
    val prefixKey = getPrefixRowKey_Data(modelId, analysis, step)
    scan.setRowPrefixFilter(prefixKey)
    scan.addFamily( Bytes.toBytes("R" ) )
    scan.setMaxVersions(1)
    // look for result index
    val infoResult = getResultInfo(modelId, analysis, step, nameResult)
    if( infoResult("name")==nameResult) {
      // add filters
      // MultipleColumnPrefixFilter
      val rindex = infoResult("index")
      scan.setFilter(new ColumnPrefixFilter(Array.concat(("r"+rindex).getBytes())))
      getRdd_Data( scan )
    } else {
      println( "Result not found")
      sc.emptyRDD
    }
  }

  def getRddResult_Data(modelId: RowKey, analysis: String, step: Double, resultPrefix: String): HBaseRDD = {
    @transient val scan = new Scan()
    val prefixKey = getPrefixRowKey_Data(modelId, analysis, step)
    scan.setRowPrefixFilter(prefixKey)
    scan.addFamily( Bytes.toBytes("R" ) )
    scan.setMaxVersions(1)
    scan.setFilter(new ColumnPrefixFilter(resultPrefix.getBytes()))
    getRdd_Data(scan)
  }

  def getResultInfo( modelId: RowKey, analysis: String, step: Double, nameResult: String): Map[String,String] = {
    val table = connection.getTable(TableName.valueOf(getTableMetaDataName))
    val row = table.get( buildGetResult_Metadata(modelId, analysis, step))
    if( row.isEmpty ) {
      println( "No row found for key '" + Bytes.toString(getPrefixRowKey_Data(modelId, analysis, step)) + "'")
      Map("name" -> "")
    } else {
      val columns = row.getFamilyMap(Bytes.toBytes("R"))

      columns.foreach(it => {
        val cf = Bytes.toString(it._1)
        val cell = Bytes.toString(it._2)
        println(cf + " = " + cell)
      })

      val default = (Array[Byte](),Array[Byte]())
      //println( "Looking for : " + nameResult)
      val col = columns.find( it => { Bytes.toString(it._1).endsWith("nm")  &&  Bytes.toString(it._2)==nameResult}).getOrElse(default)._1
      if (col.length>0) {
        val rIndex = col.slice(1,7)
        val colType = Array.concat("r".getBytes(), rIndex, "rt".getBytes())
        val rType = Bytes.toString(columns(colType))
        val colNComp = Array.concat("r".getBytes(), rIndex, "nc".getBytes())
        val rNComp = Bytes.toInt(columns(colNComp)).toString
        println( "Result " + nameResult + " found at column = " + Bytes.toString(col) + " with index = " + Bytes.toString(rIndex) +
        " has type " + rType + " number of components is " + rNComp)
        Map("name" -> nameResult,
            "index" -> Bytes.toString(rIndex),
            "prefixColumn" -> ("r"+Bytes.toString(rIndex)),
            "type" -> rType,
            "nComp" -> rNComp)
      } else {
        println( "Result '" + nameResult + "' not found")
        Map("name" -> "")
      }
    }
    //val default = ("","")

    //columns.find( Bytes.toString(_._1)  && _._2==value).getOrElse(default)._1
  }

  def getRddNodes_Data(modelId: RowKey, partition: Integer ) : HBaseRDD = {
    @transient val scan = buildScanNodes_Data( modelId, partition )
    val proto = ProtobufUtil.toScan(scan);
    val strScan = Base64.encodeBytes(proto.toByteArray());
    hconf.set(TableInputFormat.INPUT_TABLE, getTableDataName);
    hconf.set(TableInputFormat.SCAN, strScan);
    sc.newAPIHadoopRDD(hconf, classOf[TableInputFormat], classOf[HKeyRaw], classOf[HResultRaw])
  }

}

// spark-submit --class com.cimne.velassco.TestHCatalog /home/jorge/IdeaProjects/VelasscoSpark/target/VelasscoSpark-0.1-SNAPSHOT.jar --model_idx 1 --suffix "_V4CIMNE" --analysis "geometry" --timestep 1 --result "PartitionId"
// spark-submit --class com.cimne.velassco.TestHCatalog /home/jorge/IdeaProjects/VelasscoSpark/target/VelasscoSpark-0.1-SNAPSHOT.jar --model_idx 0 --suffix "_V4CIMNE" --analysis "Kratos" --timestep 21 --result "PRESSURE"