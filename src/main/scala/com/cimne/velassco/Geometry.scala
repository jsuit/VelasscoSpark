package com.cimne.velassco

object Geometry {
  type Point = (Double,Double,Double);
  type Interval = (Double,Double);
  type BBox = (Interval,Interval,Interval);


  def updI0( I: Interval, x: Double) : Interval = {
    if (I._1 > x)
      (x, I._2)
    else if (I._2 < x)
      (I._1, x)
    else I
  }

  def updI1( I1: Interval, I2: Interval) : Interval = {
    updI0( updI0( I1, I2._1 ), I2._2 )
  }

  def updBB0( bb: BBox, p: Point ) : BBox = {
    (updI0(bb._1, p._1), updI0(bb._2, p._2), updI0(bb._3, p._3))
  }

  def updBB1( bb1: BBox, bb2: BBox ) : BBox = {
    (updI1(bb1._1, bb2._1), updI1(bb1._2, bb2._2), updI1(bb1._3, bb2._3))
  }
}