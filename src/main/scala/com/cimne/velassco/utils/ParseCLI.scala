package com.cimne.velassco.utils

import org.apache.spark.SparkConf

/**
  * Created by jorge on 30/08/16.
  */
class ParseCLI( conf: SparkConf ) {
  var options = scala.collection.mutable.Map("--model_idx" -> "0", "--model_id" -> "", "--suffix" -> "",
    "--result_id" -> "", "--output_path" -> "", "--mesh_id" -> "m000001",
    "--result" -> "", "--component" -> "0", "--timestep" -> "0.0", "--analysis" -> "", "--isovalue" -> "0.0",
    "--scanner_timeout" -> "0", "--error_path" -> "" )

  def printUsage(): Unit = {
    println( "Usage: " + conf.get("spark.app.name") + " --model_idx int>=0 --suffix string " +
    "--analysis string --timestep double>=0.0 --result string --component int>=0 --isovalue double")
  }

  def getScannerTimeout = getOptionIntPos0("--scanner_timeout")

  def getOptionDouble(name: String):Double = {
    val value = try {
      options(name).toDouble
    } catch {
      case e: NumberFormatException => {
        println("option " + name + ": wrong int number format '" + options(name) + "'")
        sys.exit()
      };
      case e: Exception => {
        println("option "+ name + ": unexpected exception found: " + e.getMessage())
        sys.exit()
      }
    }
    value
  }

  def getOptionDoublePos0(name: String):Double = {
    val value = try {
      options(name).toDouble
    } catch {
      case e: NumberFormatException => {
        println("option " + name + ": wrong int number format '" + options(name) + "'")
        sys.exit()
      };
      case e: Exception => {
        println("option "+ name + ": unexpected exception found: " + e.getMessage())
        sys.exit()
      }
    }
    if (value < 0 ) {
      println("option " + name + ": expected numbrer >= 0, received '" + options(name) + "'")
      sys.exit()
    } else value
  }

  def getOptionIntPos0(name: String):Int = {
    val value = try {
      options(name).toInt
    } catch {
      case e: NumberFormatException => {
        println("option " + name + ": wrong int number format '" + options(name) + "'")
        sys.exit()
      };
      case e: Exception => {
        println("option "+ name + ": unexpected exception found: " + e.getMessage())
        sys.exit()
      }
    }
    if (value < 0 ) {
      println("option " + name + ": expected numbrer >= 0, received '" + options(name) + "'")
      sys.exit()
    } else value
  }

  def getOutputPath() = options("--output_path")

  def getErrorPath() = options("--error_path")

  def getModelIndex(): Int = getOptionIntPos0("--model_idx")

  def getModelKey(): Array[Byte] = options("--model_id").getBytes

  def getMeshId() = options("--mesh_id")

  def getTimeStep(): Double = getOptionDoublePos0("--timestep")

  def getSuffix() = options("--suffix")

  def getResultName() = options("--result")

  def getResultPrefix() = options("--result_id")

  def getResulComponent() = getOptionIntPos0("--component")

  def getAnalysisName() = options("--analysis")

  def hasOption(name:String): Boolean = options.contains(name)

  def setArgs( args : Array[String] ): Unit = {
    if( args.length % 2 == 0 ) {
      for( i <- 0 until args.length/2 ) {
        val k = args(2*i);
        val v = args(2*i+1)
        options += (k->v)
      }
    } else {
      println( "Odd number of arguments")
      printUsage()
      sys.exit()
    }
  }
}
