package com.cimne.velassco

import java.util

import org.apache.hadoop.hbase.util.Bytes

import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer

/**
  * Created by jorge on 15/06/16.
  */
object FEM {

  type Point = (Double, Double, Double)
  type Edge = (Long, Long)
  type Triangle = (Long, Long, Long)
  type TriangleEdge = (Edge, Edge, Edge)
  type Tetrahedra = (Long,Long,Long,Long)

  def getPointFromBytes(bytes: Array[Byte]): Point = (Bytes.toDouble(bytes,0), Bytes.toDouble(bytes,8), Bytes.toDouble(bytes,16))

  def getBytesFromTetrahedra( tetra: Tetrahedra  ) : Array[Byte] = {
    val result: Array[Byte] = Array.fill[Byte](32)(0)
    Array.copy( Bytes.toBytes(tetra._1), 0, result, 0, 8 )
    Array.copy( Bytes.toBytes(tetra._2), 0, result, 8, 8 )
    Array.copy( Bytes.toBytes(tetra._3), 0, result, 16, 8 )
    Array.copy( Bytes.toBytes(tetra._4), 0, result, 24, 8 )

    return result
  }

  def getKeyFromTriangle( t: Triangle ): Triangle = {
    var (n1,n2,n3) = t
    var tmp = n1
    if (n1 > n2) { tmp = n1; n1 = n2; n2 = tmp; }
    if (n2 > n3) { tmp = n2; n2 = n3; n3 = tmp; }
    if (n1 > n2) { tmp = n1; n1 = n2; n2 = tmp; }

    return new Triangle(n1, n2, n3)
  }

  def getTriangleSeqFromTetraWithKey( bytes: Array[Byte] ): Seq[(Triangle, Triangle)] = {
    var n1 = Bytes.toLong( bytes, 0 )
    var n2 = Bytes.toLong( bytes, 8 )
    var n3 = Bytes.toLong( bytes, 16 )
    var n4 = Bytes.toLong( bytes, 24 )
    return Seq(
      (getKeyFromTriangle((n1, n2, n3)), (n1, n2, n3)),
      (getKeyFromTriangle((n1, n4, n2)), (n1, n4, n2)),
      (getKeyFromTriangle((n2, n4, n3)), (n2, n4, n3)),
      (getKeyFromTriangle((n1, n3, n4)), (n1, n3, n4)) )
  }

  def getTriangleSeqFromTetra(bytesTetra: Array[Byte] ): Seq[Triangle] = {
    if(bytesTetra.length >= 32) {
      var n1 = Bytes.toLong(bytesTetra, 0)
      var n2 = Bytes.toLong(bytesTetra, 8)
      var n3 = Bytes.toLong(bytesTetra, 16)
      var n4 = Bytes.toLong(bytesTetra, 24)
      Seq((n1, n2, n3), (n1, n4, n2),(n2, n4, n3), (n1, n3, n4))
    }
    else {
      println("[getTriangleSeqFromTetra] -- the input bytes is not a tetrahedra, has length = " + bytesTetra.length)
      Seq()
    }
  }

  def getEdgeSeqFromTetra(bytes: Array[Byte]): Seq[Edge] = {
    var n1 = Bytes.toLong( bytes, 0 )
    var n2 = Bytes.toLong( bytes, 8 )
    var n3 = Bytes.toLong( bytes, 16 )
    var n4 = Bytes.toLong( bytes, 24 )
    val s1 = Seq(n1,n2,n3,n4).sorted
    return Seq( (s1(0), s1(1)),(s1(0), s1(2)),(s1(0), s1(3)),(s1(1), s1(2)),(s1(1), s1(3)),(s1(2), s1(3)) )
  }

  def insertBoundaryFacesFromTetra( bytes: Array[Byte], boundary: scala.collection.mutable.HashMap[Triangle,Triangle]):Unit = {
    val n1 = Bytes.toLong( bytes, 0 )
    val n2 = Bytes.toLong( bytes, 8 )
    val n3 = Bytes.toLong( bytes, 16 )
    val n4 = Bytes.toLong( bytes, 24 )

    val t1: Triangle = (n1, n2, n3)
    val k1: Triangle = getKeyFromTriangle(t1)
    if( boundary.contains( k1 ) ) boundary.remove( k1 )
    else boundary(k1) = t1
    val t2: Triangle = (n1, n4, n2)
    val k2: Triangle = getKeyFromTriangle(t2)
    if( boundary.contains( k2 ) ) boundary.remove( k2 )
    else boundary(k2) = t2
    val t3: Triangle = (n2, n4, n3)
    val k3: Triangle = getKeyFromTriangle(t3)
    if( boundary.contains( k3 ) ) boundary.remove( k3 )
    val t4: Triangle = (n1, n3, n4)
    val k4: Triangle = getKeyFromTriangle(t4)
    if( boundary.contains( k4 ) ) boundary.remove( k4 )
    else boundary(k4) = t4
  }

  def getCut(x1: Double, x2: Double, x:Double): Double = (x-x1)/(x2-x1)

  def interp(x1: Double, x2: Double, a: Double): Double = x1 + (x2-x1)*a

  def interp(p1: Point, p2: Point, a: Double): Point = (interp(p1._1, p2._1, a),interp(p1._2, p2._2, a),interp(p1._3, p2._3, a))

  def interpEdge( node1:(Point, Long, Double), node2: (Point, Long, Double), iso: Double, mapEdges: HashMap[Edge,Point] ): Edge = {
    var key:Edge = (0,0)
    if (node1._2 < node2._2) {
      key = (node1._2,node2._2)
      if(!mapEdges.contains(key)) mapEdges+=(key -> interp(node1._1, node2._1, getCut(node1._3, node2._3, iso)))
      key
    } else {
      key = (node2._2,node1._2)
      if(!mapEdges.contains(key)) mapEdges+=(key -> interp(node2._1, node1._1, getCut(node2._3, node1._3, iso)))
      key
    }
  }

  // http://paulbourke.net/geometry/polygonise/
  def genIsoTetra(tetra: Array[Byte], iso: Double,
                  prefixR: Array[Byte], component: Int,
                  mapResults: util.NavigableMap[Array[Byte],Array[Byte]],
                  mapMesh: util.NavigableMap[Array[Byte],Array[Byte]],
                  mapEdges: HashMap[Edge,Point], mapTriangles:ListBuffer[TriangleEdge] ): Unit = {
    val n1 = Bytes.toLong( tetra, 0 )
    val n2 = Bytes.toLong( tetra, 8 )
    val n3 = Bytes.toLong( tetra, 16 )
    val n4 = Bytes.toLong( tetra, 24 )

    // rRRRRRRvl_id
    var r1 = 0.0;
    var r2 = 0.0;
    var r3 = 0.0;
    var r4 = 0.0;
    try {
      r1 = Bytes.toDouble(mapResults.get(Array.concat(prefixR, Bytes.toBytes(n1))), component * 8)
      r2 = Bytes.toDouble(mapResults.get(Array.concat(prefixR, Bytes.toBytes(n2))), component * 8)
      r3 = Bytes.toDouble(mapResults.get(Array.concat(prefixR, Bytes.toBytes(n3))), component * 8)
      r4 = Bytes.toDouble(mapResults.get(Array.concat(prefixR, Bytes.toBytes(n4))), component * 8)
    } catch {
      case e: Exception => {
        println("[genIsoTetra] -- unexpected exception: " + e.getMessage())
        println( "prefixR = " + Bytes.toString(prefixR))
        return
      }
    }
    var triIndex: Int = 0
    /*
    Determine which of the 16 cases we have given which vertices
    are above or below the isosurface
    */
    if (r1 < iso) triIndex |= 1
    if (r2 < iso) triIndex |= 2
    if (r3 < iso) triIndex |= 4
    if (r4 < iso) triIndex |= 8

    val prefixNode = "c000001_".getBytes
    val crd1 = getPointFromBytes( mapMesh.get(Array.concat(prefixNode, tetra.slice(0, 8))))
    val crd2 = getPointFromBytes( mapMesh.get(Array.concat(prefixNode, tetra.slice(8, 16))))
    val crd3 = getPointFromBytes( mapMesh.get(Array.concat(prefixNode, tetra.slice(16, 24))))
    val crd4 = getPointFromBytes( mapMesh.get(Array.concat(prefixNode, tetra.slice(24, 32))))
    val p1 = (crd1, n1, r1)
    val p2 = (crd2, n2, r2)
    val p3 = (crd3, n3, r3)
    val p4 = (crd4, n4, r4)
    triIndex match {
        case 0x00 | 0x0F => {}
        case 0x0E | 0x01 => {
          val edgeCut1:Edge = interpEdge( p1, p2, iso, mapEdges )
          val edgeCut2:Edge = interpEdge( p1, p3, iso, mapEdges )
          val edgeCut3:Edge = interpEdge( p1, p4, iso, mapEdges )
          if (p1._3 > p2._3 ) {
            mapTriangles += ((edgeCut1, edgeCut2, edgeCut3))
          } else {
            mapTriangles += ((edgeCut1, edgeCut3, edgeCut2))
          }
        }
        case 0x0D | 0x02 => {
          val edgeCut1:Edge = interpEdge( p2, p1, iso, mapEdges )
          val edgeCut2:Edge = interpEdge( p2, p4, iso, mapEdges )
          val edgeCut3:Edge = interpEdge( p2, p3, iso, mapEdges )
          if (p2._3 > p1._3 ) {
            mapTriangles += ((edgeCut1, edgeCut2, edgeCut3))
          } else {
            mapTriangles += ((edgeCut1, edgeCut3, edgeCut2))
          }
        }
        case 0x0C | 0x03 => {
          val edgeCut1: Edge = interpEdge(p1, p4, iso, mapEdges)
          val edgeCut2: Edge = interpEdge(p1, p3, iso, mapEdges)
          val edgeCut3: Edge = interpEdge(p2, p4, iso, mapEdges)
          val edgeCut4: Edge = interpEdge(p2, p3, iso, mapEdges)
          if( p1._3 > p4._3 ) {
            mapTriangles += ((edgeCut1, edgeCut2, edgeCut3), (edgeCut2, edgeCut4, edgeCut3))
          } else {
            mapTriangles += ((edgeCut1, edgeCut3, edgeCut2), (edgeCut2, edgeCut3, edgeCut4))
          }
        }
        case 0x0B | 0x04 => {
          val edgeCut1:Edge = interpEdge( p3, p1, iso, mapEdges )
          val edgeCut2:Edge = interpEdge( p3, p2, iso, mapEdges )
          val edgeCut3:Edge = interpEdge( p3, p4, iso, mapEdges )
          if (p3._3 > p1._3) {
            mapTriangles += ((edgeCut1, edgeCut3, edgeCut2))
          } else {
            mapTriangles += ((edgeCut1, edgeCut2, edgeCut3))
          }
        }
        case 0x0A | 0x05 => {
          val edgeCut1: Edge = interpEdge(p1, p2, iso, mapEdges)
          val edgeCut2: Edge = interpEdge(p3, p4, iso, mapEdges)
          val edgeCut3: Edge = interpEdge(p1, p4, iso, mapEdges)
          val edgeCut4: Edge = interpEdge(p2, p3, iso, mapEdges)
          if (p1._3 > p2._3) {
            mapTriangles += ((edgeCut1, edgeCut3, edgeCut2), (edgeCut1, edgeCut2, edgeCut4))
          } else {
            mapTriangles += ((edgeCut1, edgeCut2, edgeCut3), (edgeCut1, edgeCut4, edgeCut2))
          }
        }
        case 0x09 | 0x06 => {
          val edgeCut1: Edge = interpEdge(p1, p2, iso, mapEdges)
          val edgeCut2: Edge = interpEdge(p2, p4, iso, mapEdges)
          val edgeCut3: Edge = interpEdge(p3, p4, iso, mapEdges)
          val edgeCut4: Edge = interpEdge(p1, p3, iso, mapEdges)
          if (p1._3 > p2._3) {
            mapTriangles += ((edgeCut1, edgeCut3, edgeCut2), (edgeCut1, edgeCut4, edgeCut3))
          } else {
             mapTriangles += ((edgeCut1, edgeCut2, edgeCut3), (edgeCut1, edgeCut3, edgeCut4))
          }
        }
        case 0x07 | 0x08 => {
          val edgeCut1:Edge = interpEdge( p4, p1, iso, mapEdges )
          val edgeCut2:Edge = interpEdge( p4, p3, iso, mapEdges )
          val edgeCut3:Edge = interpEdge( p4, p2, iso, mapEdges )
          if (p4._3 > p1._3) {
            mapTriangles += ((edgeCut1, edgeCut2, edgeCut3))
          } else {
            mapTriangles += ((edgeCut1, edgeCut3, edgeCut2))
          }
        }
    }
  }
}
